10.times do
  Contact.create(
    name: Faker::Name.name, 
    email: Faker::Internet.email, 
    phone: Faker::Number.number(digits: 12)
  )
end