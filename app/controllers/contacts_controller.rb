class ContactsController < ApplicationController
  def index
    @contacts = Contact.order(created_at: :desc) 

    respond_to do |format|
      format.html
      format.json { render json: @contacts }
    end
  end

  def show
    respond_to do |format|  
      begin
        @contact = Contact.find(params[:id])
        format.json { render json: @contact } 
      rescue => e
        format.json { render json: {message: e}, status: :bad_request}
      end
    end
  end

  def new
    @contact = Contact.new()

    respond_to do |format|  
      format.js  
    end
  end

  def edit
    @contact = Contact.find(params[:id])

    respond_to do |format|  
      format.js  
    end
  end

  def create
    respond_to do |format|
      begin
        @contact = Contact.new(contact_params)
        @contact.save!
        @alertMessage = 'Contact created'
        format.js
        format.json { render json: @contact } 
      rescue => e
        format.js
        format.json { render json: {message: e}, status: :bad_request}
      end
    end
  end

  def update
    respond_to do |format|
      begin
        @contact = Contact.find(params[:id])
        @contact.update!(contact_params)
        @alertMessage = 'Contact updated' 
        format.js
        format.json { render json: @contact } 
      rescue => e
        format.js
        format.json { render json: {message: e}, status: :bad_request}
      end
    end
  end

  def destroy
    respond_to do |format|
      begin
        @contact = Contact.find(params[:id])
        @contact.destroy!
        @alertMessage = 'Contact deleted'
        format.json { render json: @contact } 
      rescue => e
        @alertMessage = 'Contact not found'
        format.js
        format.json { render json: {message: e}, status: :bad_request}
      end
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone)
  end
end
