global.setFormAlert = function(alertName='', alertEmail='', alertPhone='') {
  document.getElementById('alert-name').innerHTML = alertName
  document.getElementById('alert-email').innerHTML = alertEmail
  document.getElementById('alert-phone').innerHTML = alertPhone
}

global.cleanForm = function () {
  const inputs = document.getElementById('modal-body').getElementsByTagName('input')
  
  for (e of inputs) {
    e.value = ''
  }
}

global.setAlertMessage = function (message = '') {
  document.getElementById('alert-message').innerHTML = message 
}

document.getElementById('form-modal').addEventListener('hidden.bs.modal', event => {
  document.getElementById('form').innerHTML = ''
})

global.setModalTitle = function (title) {
  document.getElementById('modal-title').innerHTML = title
}