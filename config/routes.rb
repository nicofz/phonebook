Rails.application.routes.draw do
  root "contacts#index"
  # get '/contacts', to: 'contacts#index'
  # get '/contacts/new', to: 'contacts#new'
  # get '/contacts/:id', to: 'contacts#edit', as: 'contact'
  # post '/contacts', to: 'contacts#create'
  # delete '/contacts/:id', to: 'contacts#destroy'

  resources :contacts
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
